<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="utils.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>HOME</title>
		<%@include file="header.jsp" %>
	</head>
	<body>
		<div align="center">
			<h2>HOME</h2>
			<h3>Hola <%=username%>!</h3>
			<br>
			
			<table align="center">
				<tr>
					<td>
						<form action="loadPage" method="post">
							<input type="hidden" name="page" value="<%=Constants.PARAM_PAG1%>" />
							<input type="submit" value="Pagina 1">
						</form>
					</td>
					<td>
						<form action="loadPage" method="post">
							<input type="hidden" name="page" value="<%=Constants.PARAM_PAG2%>" />
							<input type="submit" value="Pagina 2">
						</form>
					</td>
					<td>
						<form action="loadPage" method="post">
							<input type="hidden" name="page" value="<%=Constants.PARAM_PAG3%>" />
							<input type="submit" value="Pagina 3">
						</form>
					</td>
				</tr>
			</table>
			<br/>
			<%@include file="logout.jsp" %>
    	</div>
	</body>
</html>
