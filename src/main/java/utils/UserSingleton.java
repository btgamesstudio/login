package utils;

import java.util.HashMap;
import java.util.Map;

import objects.User;

// Simulamos un dao.
public class UserSingleton {
	
	private static UserSingleton instance = null;
	
	private Map<String, User> users;
	
	protected UserSingleton() {
		// Metodo privado para no poder crear objetos.
	}
	
	public static UserSingleton getInstance() {
		if (instance == null) {
			instance = new UserSingleton();
		}
		return instance;
	}
	
	public User getUser(String username) {
		return getUsers().get(username);
	}
	
	public Map<String, User> getUsers() {
		if (users == null) {
			users = new HashMap<String, User>();
			users.put("admin", new User("admin", "admin", Constants.ROL_PAG1, Constants.ROL_PAG2, Constants.ROL_PAG3));
			users.put("user1", new User("user1", "pass1", Constants.ROL_PAG1));
			users.put("user2", new User("user2", "pass2", Constants.ROL_PAG2));
			users.put("user3", new User("user3", "pass3", Constants.ROL_PAG3));
			users.put("user4", new User("user4", "pass4"));
		}
		
		return users;
	}
}
