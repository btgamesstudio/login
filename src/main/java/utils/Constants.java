package utils;

public class Constants {

	private Constants() {}
	
	public static int COOKIE_MAX_AGE = 60 * 5;
	
	// Roles.
	public static final String ROL_PAG1 = "ROL_PAG1";
	public static final String ROL_PAG2 = "ROL_PAG2";
	public static final String ROL_PAG3 = "ROL_PAG3";
	
	// Parametros de pagina.
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_HOME = "home";
	public static final String PARAM_PAG1 = "pag1";
	public static final String PARAM_PAG2 = "pag2";
	public static final String PARAM_PAG3 = "pag3";
	
	// Paginas.
	public static final String JSP_LOGIN = "/WEB-INF/pages/login.jsp";
	public static final String JSP_HOME = "/WEB-INF/pages/home.jsp";
	public static final String JSP_PAG1 = "/WEB-INF/pages/pag1.jsp";
	public static final String JSP_PAG2 = "/WEB-INF/pages/pag2.jsp";
	public static final String JSP_PAG3 = "/WEB-INF/pages/pag3.jsp";
}
