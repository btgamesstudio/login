package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objects.User;
import utils.Constants;
import utils.UserSingleton;

@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.setContentType("text/html");
		
		// Obtenemos los parametros de la request.
		String username = request.getParameter(Constants.PARAM_USERNAME);
		String password = request.getParameter(Constants.PARAM_PASSWORD);
		
		// Validamos el usuario.
		RequestDispatcher requestDispatcher = null;
		User user = UserSingleton.getInstance().getUser(username);
		if (user != null && user.getPassword().equals(password)) {
			// Usuario validado.
			Cookie cookie = new Cookie(Constants.PARAM_USERNAME, username);
			cookie.setMaxAge(Constants.COOKIE_MAX_AGE); // Seteamos que la cookie expira en 5 minutos.
			response.addCookie(cookie);
			
			// Seteamos al usuario en un parametro.
			request.setAttribute(Constants.PARAM_USERNAME, username);
			requestDispatcher = getServletContext().getRequestDispatcher(Constants.JSP_HOME);
		} else {
			// Usuario incorrecto.
			requestDispatcher = request.getRequestDispatcher(Constants.JSP_LOGIN);
			PrintWriter out = response.getWriter();
			out.println("<div align=\"center\"><font color=\"red\">Usuario o contraseña incorrecta.</font></div>\n");
		}
		requestDispatcher.include(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie loginCookie = LoadPageServlet.updateLoginCookie(request, response, Constants.COOKIE_MAX_AGE);
		
		RequestDispatcher requestDispatcher = null;
		if (loginCookie == null) {
			requestDispatcher = request.getRequestDispatcher(Constants.JSP_LOGIN);
		} else {
			requestDispatcher = request.getRequestDispatcher(Constants.JSP_HOME);
		}
		requestDispatcher.include(request, response);
	}
}
