package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objects.User;
import utils.Constants;
import utils.UserSingleton;

@WebServlet("/loadPage")
public class LoadPageServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final String PAGE = "page";

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Obtenemos la pagina a la que queremos navegar.
		String page = request.getParameter(PAGE);
		// Obtenemos la cookie.
		Cookie loginCookie = updateLoginCookie(request, response, Constants.COOKIE_MAX_AGE);
		
		// Verificamos que el usuario de la cookie tiene acceso a la pantalla.
		RequestDispatcher requestDispatcher = null;
		if (loginCookie == null) {
			requestDispatcher = request.getRequestDispatcher(Constants.JSP_LOGIN);
		} else if (Constants.PARAM_HOME.equals(page)) {
			requestDispatcher = getServletContext().getRequestDispatcher(Constants.JSP_HOME);
		} else if (Constants.PARAM_PAG1.equals(page) && hasRole(loginCookie.getValue(), Constants.ROL_PAG1)) {
			requestDispatcher = getServletContext().getRequestDispatcher(Constants.JSP_PAG1);
		} else if (Constants.PARAM_PAG2.equals(page) && hasRole(loginCookie.getValue(), Constants.ROL_PAG2)) {
			requestDispatcher = getServletContext().getRequestDispatcher(Constants.JSP_PAG2);
		} else if (Constants.PARAM_PAG3.equals(page) && hasRole(loginCookie.getValue(), Constants.ROL_PAG3)) {
			requestDispatcher = getServletContext().getRequestDispatcher(Constants.JSP_PAG3);
		} else {
			PrintWriter out = response.getWriter();
			out.println("<div align=\"center\"><font color=\"red\">Acceso denegado.</font></div>\n");
			requestDispatcher = getServletContext().getRequestDispatcher(Constants.JSP_HOME);
		}
		requestDispatcher.include(request, response);
		//requestDispatcher.forward(request, response);
	}
	
	private boolean hasRole(String username, String rol) {
		// Comprobamos que el usuario tenga el rol.
		if (username != null && rol != null) {
			User user = UserSingleton.getInstance().getUser(username);
			if (user != null && user.getRoles().contains(rol)) {
				return Boolean.TRUE;
			}
		}
		
		return Boolean.FALSE;
	}
	
	public static Cookie updateLoginCookie(HttpServletRequest request, HttpServletResponse response, int maxAge) {
		Cookie[] cookies = request.getCookies();
        Cookie loginCookie = null;
        
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(Constants.PARAM_USERNAME)) {
                	// Seleccionamos la cookie del usuario y le actualizamos su tiempo de vida.
                	loginCookie = cookie;
                	cookie.setMaxAge(maxAge);
                    response.addCookie(cookie);
                    // En cada navegacion seteamos al usuario en un parametro.
        			request.setAttribute(Constants.PARAM_USERNAME, cookie.getValue());
                    break;
                }
            }
        }
        return loginCookie;
	}
}
