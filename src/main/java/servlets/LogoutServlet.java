package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.Constants;

@WebServlet("/logoutServlet")
public class LogoutServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Hacemos caducar la cookie.
		LoadPageServlet.updateLoginCookie(request, response, 0);
		// Redirigimos al login.
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(Constants.JSP_LOGIN);
		requestDispatcher.include(request, response);
	}
}
