package objects;

import java.util.ArrayList;
import java.util.List;

public class User {
	
	private String username;
	private String password;
	private List<String> roles;
	
	public User(String username, String password, String ... roles) {
		this.username = username;
		this.password = password;
		
		for (String rol: roles) {
			getRoles().add(rol);
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getRoles() {
		if (roles == null) {
			roles = new ArrayList<String>();
		}
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}
